Documentor
==========

**Documentor** is a web based tool, that enables easier documentation of meetings.


Meta
----

Author:
    k3k5

Status:
    maintained, in development

Version:
    0.1
