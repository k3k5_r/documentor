from django.db import models

# Create your models here.
class Redmine(models.Model):
    url = models.URLField()
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    is_admin_account = models.BooleanField(default=False)

    def __str__(self):
        return self.url

class RedmineProject(models.Model):
    redmine_instance = models.ForeignKey(Redmine, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    project_slug = models.CharField(max_length=255)
    homepage = models.URLField(null=True, blank=True)
    is_private = models.BooleanField(default=True)

    def __str__(self):
        return self.name
