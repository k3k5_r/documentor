from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model

from .src.connector import RedmineConnector
from .models import RedmineProject
from earned_value_analysis.models import EarnedValueAnalysis, ProjectMetaInformation

# Create your views here.
@login_required
def index(request):
    instance = RedmineConnector()
    print(instance)
    return HttpResponse("You're looking at question %s." % str(instance))

@login_required
def get_all_redmine_projects(request):
    instance = RedmineConnector()
    instance.get_all_redmine_projects()
    return HttpResponse("You're looking at question %s." % str(instance))

class DashboardView():
    @login_required
    def index(request):
        from meeting_notes.src.planner.controller import SprintController
        from meeting_notes.models import Sprint, TimeEstimate
        import json
        import datetime
        from django.core.serializers.json import DjangoJSONEncoder

        controller = SprintController(request.user)
        total_slots = int(controller.get_available_time_slots_in_sprint())
        time_slots = controller.set_items_to_time_slots()

        projects = RedmineProject.objects.all()
        try:
            earned_value = EarnedValueAnalysis.objects.latest()
        except:
            earned_value = list()

        context = {
            "projects": projects[:30],
            "all_projects": projects,
            "earned_value": earned_value,
            'total_slots': total_slots,
            'time_slots': time_slots,
            'num_of_users': len(get_user_model().objects.filter(is_staff=True))
        }
        return render(request, 'dashboard/index.html', context)

    @login_required
    def gantt_data(request):
        from meeting_notes.src.planner.controller import SprintController
        from meeting_notes.models import Sprint, TimeEstimate
        import json
        import datetime
        from django.core.serializers.json import DjangoJSONEncoder

        controller = SprintController(request.user)
        total_slots = int(controller.get_available_time_slots_in_sprint())
        time_slots = controller.set_items_to_time_slots()
        gantt_data_intermed = list()
        gantt_data = list()

        current_sprint = Sprint.objects.get(sprint_id=controller.get_current_sprint_id())
        start_and_end_of_sprint = datetime.datetime.combine(controller.get_start_and_end_for_current_sprint().get('start_date'), datetime.time(hour=8))
        current_datetime = start_and_end_of_sprint
        for day in time_slots:
            for task in day:
                if task:
                    current = TimeEstimate.objects.get(pk=int(task))
                    dependencies = str(current.note.is_dependent_on.id) if current.note.is_dependent_on is not None else None
                    resource = current.note.agenda_item.title if current.note.agenda_item is not None else 'Other'
                    gantt_data_intermed.append([
                        current.id,
                        current.note.text,
                        resource,
                        current_datetime,
                        current_datetime + datetime.timedelta(hours=SprintController.MINIMAL_TIME_INTERVAL),
                        None,
                        0,
                        dependencies
                    ])
                current_datetime += datetime.timedelta(hours=SprintController.MINIMAL_TIME_INTERVAL)
            current_datetime = current_datetime + datetime.timedelta(days=1)
            current_datetime = datetime.datetime.combine(current_datetime, datetime.time(hour=8))
        
        full_list = [item for sublist in time_slots for item in sublist]
        id_list = set(full_list)
        test = [x[0] for x in gantt_data_intermed]
        
        for item in id_list:
            if item:
                first_index = test.index(item)
                last_index = len(test) - test[::-1].index(item) - 1

                first_item = gantt_data_intermed[first_index]
                last_item = gantt_data_intermed[last_index]

                first_item[0] = str(TimeEstimate.objects.get(pk=first_item[0]).note.id)
                first_item[4] = last_item[4]
                first_item[3] = first_item[3]

                gantt_data.append(first_item)

        gantt_data.sort(key=SprintController.get_start_key)

        gantt_data = json.dumps(
            gantt_data,
            sort_keys=True,
            indent=1,
            cls=DjangoJSONEncoder
        )

        return HttpResponse(gantt_data, {'Content-Type': 'application/json'})