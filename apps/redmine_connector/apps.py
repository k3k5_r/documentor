from django.apps import AppConfig


class RedmineConnectorConfig(AppConfig):
    name = 'redmine_connector'
