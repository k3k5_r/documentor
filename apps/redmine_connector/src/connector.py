from django import db
import pandas as pd

from redminelib import Redmine as RedmineAPI

from ..models import *

class RedmineConnector(object):

    def __init__(self, identifier="test"):
        self.local_project = RedmineProject.objects.get(project_slug__exact=identifier)
        self.connection = RedmineAPI(
            self.local_project.redmine_instance.url,
            username=self.local_project.redmine_instance.username,
            password=self.local_project.redmine_instance.password
        )
        self.project = self.connection.project.get(self.local_project.project_slug)
        self.custom_fields = [dict(x) for x in self.project.custom_fields]

    def __str__(self):
        return str(dir(self.project))

    def get_all_redmine_projects(self):
        all_projects = self.connection.project.all()
        internal_projects = [proj for proj in all_projects if not proj.name.endswith(' - E') and proj.name.startswith('J -')]
        RedmineProject.objects.all().delete()
        for project in internal_projects:
            RedmineProject(
                redmine_instance=Redmine.objects.filter(id=1)[0],
                name=project.name,
                project_slug=project.identifier,
            ).save()
