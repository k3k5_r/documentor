from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Note)
admin.site.register(Comment)
admin.site.register(AgendaItem)
admin.site.register(TimeEstimate)
admin.site.register(SprintUserSettings)
admin.site.register(Sprint)

class NoteInline(admin.TabularInline):
    model = Note
    extra = 10

class AgendaItemInline(admin.TabularInline):
    model = AgendaItem
    extra = 5

class MeetingAdmin(admin.ModelAdmin):
    inlines = [AgendaItemInline, NoteInline]

admin.site.register(Meeting, MeetingAdmin)
admin.site.register(TodoistConnector)
