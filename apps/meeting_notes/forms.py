from django.forms import ModelForm, Select, SelectMultiple, RadioSelect, TextInput, DateTimeInput
from django import forms
from django.contrib.admin import widgets

from .models import Meeting, AgendaItem, Note, TodoistConnector, Comment, TimeEstimate


class DateTimeInput(forms.DateTimeInput):
    input_type = "datetime-local"

    def __init__(self, **kwargs):
        kwargs["format"] = "%Y-%m-%dT%H:%M"
        super().__init__(**kwargs)

class MeetingForm(ModelForm):

    class Meta:
        model = Meeting
        fields = "__all__"
        widgets = {
            'start': DateTimeInput(attrs={'type': 'datetime-local'}),
            'end': DateTimeInput(attrs={'class': 'datetime-local'}),
            'creator': forms.HiddenInput(),
            'status': Select(attrs={'class': 'ui search dropdown'}),
            'meeting_redmine_project': Select(attrs={'class': 'ui search dropdown'}),
            'participants': SelectMultiple(attrs={'multiple': '', 'class': 'ui search dropdown'})
        }

class AgendaItemForm(ModelForm):

    class Meta:
        model = AgendaItem
        fields = "__all__"
        exclude = ('done_timestamp', )
        widgets = {
            'important_score': Select(attrs={'class': 'ui search dropdown'}),
            'project_section': Select(attrs={'class': 'ui search dropdown'}),
            'meeting': forms.HiddenInput(),
        }


class NoteForm(ModelForm):

    class Meta:
        model = Note
        fields = "__all__"
        exclude = ('done_timestamp', )
        widgets = {
            'creator': forms.HiddenInput(),
            'meeting': forms.HiddenInput(),
            'agenda_item': Select(attrs={'class': 'ui search dropdown'}),
            'is_dependent_on': Select(attrs={'class': 'ui search dropdown'}),
            'note_type': Select(attrs={'class': 'ui search dropdown'}),
            'project_section': Select(attrs={'class': 'ui search dropdown'}),
            'assigned_to': Select(attrs={'class': 'ui search dropdown'}),
            'due_date': DateTimeInput(attrs={'type': 'datetime-local'}),
            'submit_type': forms.HiddenInput(),
        }


class CommentForm(ModelForm):

    class Meta:
        model = Comment
        fields = "__all__"
        widgets = {
            'creator': forms.HiddenInput(),
            'timestamp': forms.HiddenInput(),
            'note': forms.HiddenInput(),
        }


class TodoistForm(ModelForm):

    class Meta:
        model = TodoistConnector
        fields = "__all__"


class TimeEstimateForm(ModelForm):

    class Meta:
        model = TimeEstimate
        fields = "__all__"
        exclude = ("time_estimate_in_slots", )
        widgets = {
            'note': forms.HiddenInput(),
            'user': forms.HiddenInput(),
        }
