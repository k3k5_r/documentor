from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.http import HttpResponse, FileResponse
from django.core import mail
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model
from django.conf import settings
from django.db.models import Q

from redmine_connector.models import RedmineProject
from .models import Meeting, AgendaItem, Note, TodoistConnector, Comment, TimeEstimate, SprintUserSettings
from .forms import MeetingForm, AgendaItemForm, NoteForm, CommentForm, TimeEstimateForm
from django.views.decorators.csrf import csrf_exempt
from django.utils.dateparse import parse_date

from ics import Calendar, Event

import datetime

class MeetingView():
    @staticmethod
    def create_ical(meeting_id: int):
        meeting = Meeting.objects.get(pk=meeting_id)
        c = Calendar()
        e = Event()
        e.name = meeting.title
        e.begin = meeting.start
        if meeting.end:
            e.end = meeting.end
        c.events.add(e)
        c.events
        filename = settings.MEDIA_ROOT + "/ics_files/" + str(meeting.id) + "_" + str(meeting.title) + '.ics'
        with open(filename, 'w') as my_file:
            my_file.writelines(c)

        return filename

    @login_required
    def index(request):
        meetings = Meeting.objects.filter(Q(creator=request.user) | Q(participants=request.user))

        context = {
            "meetings": set(meetings)
        }

        return render(request, 'meetings/index.html', context)

    @login_required
    def create(request):
        form = MeetingForm(request.POST.copy() or None, initial={'creator': request.user})

        if request.method == 'POST':
            form.data['start'] = datetime.datetime.strptime(form.data['start'], "%Y-%m-%dT%H:%M")
            form.data['end'] = datetime.datetime.strptime(form.data['end'], "%Y-%m-%dT%H:%M") if form.data['end'] else None

        if form.is_valid():
            meeting = form.save()
            return redirect(reverse('agenda_item.create', args=(meeting.pk, )))
        return render(request, 'meetings/edit.html', {'form': form, 'section': 'meeting'})

    @login_required
    def edit(request, id):
        instance = get_object_or_404(Meeting, id=id)
        form = MeetingForm(request.POST.copy() or None, instance=instance)

        if request.method == 'POST':
            form.data['start'] = datetime.datetime.strptime(form.data['start'], "%Y-%m-%dT%H:%M")
            form.data['end'] = datetime.datetime.strptime(form.data['end'], "%Y-%m-%dT%H:%M") if form.data['end'] else None

        if form.is_valid():
            form.save()
            return redirect(reverse('agenda_item.create', args=(id, )))

        return render(request, 'meetings/edit.html', {
            'form': form,
            'meeting_id': instance.id,
            'section': 'meeting'
        })

    @login_required
    def send_invitation_mail(request, meeting_id: int):
        subject = 'Neue Meeting-Einladung'
        meeting = Meeting.objects.get(pk=meeting_id)
        html_message = render_to_string('mail/default_template.html', {'meeting': meeting, 'agenda_items': AgendaItem.objects.filter(meeting=meeting)})
        plain_message = strip_tags(html_message)
        from_email = 'From <from@example.com>'
        to = 'to@example.com'

        msg = EmailMultiAlternatives(subject, plain_message, from_email, [to])
        msg.attach_alternative(html_message, "text/html")
        msg.attach('meeting.ics', MeetingView.create_ical(meeting_id))
        msg.send()

        return HttpResponse('Mail successfully sent')

    @login_required
    def send_notes_pdf(request, meeting_id: int):
        subject = 'Neues Meeting-Protokoll'
        meeting = Meeting.objects.get(pk=meeting_id)
        html_message = render_to_string('mail/default_notes.html', {'meeting': meeting, 'agenda_items': AgendaItem.objects.filter(meeting=meeting)})
        plain_message = strip_tags(html_message)
        from_email = 'From <from@example.com>'
        to = 'to@example.com'

        msg = EmailMultiAlternatives(subject, plain_message, from_email, [to])
        msg.attach_alternative(html_message, "text/html")
        msg.attach('meeting.pdf', Pdf.create(request, meeting_id))
        msg.send()

        return HttpResponse('Mail successfully sent')

    @login_required
    @csrf_exempt
    def delete(request, id: int):
        if request.method == 'POST':
            meeting = Meeting.objects.get(pk=id)
            if meeting.creator == request.user:
                meeting.delete()
                return HttpResponse(200)
            return HttpResponse(401)
        return HttpResponse(500)

    @login_required
    @csrf_exempt
    def close(request, id: int):
        if request.method == 'POST':
            instance = Meeting.objects.get(pk=id)
            instance.is_closed = not instance.is_closed
            instance.status = 'DONE'
            instance.save()
            return HttpResponse(200)
        return HttpResponse(500)

    @login_required
    def show_finish_modal(request, meeting_id):

        data = request.POST if request.method == 'POST' else None

        if request.method == 'POST':
            if data.get('send_invitations', False) == 'on':
                MeetingView.send_invitation_mail(request, meeting_id)

            if data.get('send_pdfs', False) == 'on':
                MeetingView.send_notes_pdf(request, meeting_id)
            
            return redirect(reverse('meetings.index'))

        return render(request, 'meetings/finish_modal.html', context={
            'meeting_id': meeting_id
        })


class AgendaItemView():
    @login_required
    def index(request):
        agenda_items = AgendaItem.objects.all()

        context = {
            "agenda_items": agenda_items
        }

        return render(request, 'agenda_items/index.html', context)

    @login_required
    def create(request, meeting_id):
        form = AgendaItemForm(request.POST or None, initial={'meeting': meeting_id})
        instance = AgendaItem.objects.filter(meeting=Meeting.objects.get(pk=meeting_id)).order_by('-important_score')
        if form.is_valid():
            agenda_item = form.save()
            return redirect(reverse('agenda_item.create', args=(meeting_id, )))

        return render(request, 'agenda_items/edit.html', {
            'form': form,
            'section': 'agenda_item',
            'agenda_items': instance,
            'meeting_id': meeting_id
        })

    @login_required
    def edit(request, agenda_item_id):
        instance = get_object_or_404(AgendaItem, id=agenda_item_id)
        data = request.POST if request.method == 'POST' else None

        form = AgendaItemForm(data, instance=instance)

        if form.is_valid():
            agenda_item = form.save()
            return redirect(reverse('agenda_item.create', args=(instance.meeting.pk, )))

        return render(request, 'agenda_items/edit_form.html', {
            'form': form,
            'form_action': '/meetings/agenda_items/edit/' + str(agenda_item_id) + "/"
        })

    @login_required
    @csrf_exempt
    def delete(request, id: int):
        if request.method == 'POST':
            AgendaItem.objects.get(pk=id).delete()
            return redirect('/meetings/agenda_items/index')
        return HttpResponse(500)

    @login_required
    @csrf_exempt
    def mark_done(request, id: int):
        if request.method == 'POST':
            agenda_item = get_object_or_404(AgendaItem, id=id)
            agenda_item.is_done = True
            agenda_item.done_timestamp = datetime.datetime.now()
            agenda_item.save()
            return HttpResponse(200)
        return HttpResponse(500)


class NotesView():
    @login_required
    def index(request, meeting_id):
        agenda_items = AgendaItem.objects.filter(meeting=meeting_id)

        context = {
            "agenda_items": agenda_items
        }

        return render(request, 'notes/index.html', context)

    @login_required
    def own(request):

        context = {
            'assigned': Note.objects.filter(assigned_to=request.user),
            'created': Note.objects.filter(creator=request.user)
        }

        return render(request, 'notes/own.html', context)

    @login_required
    def create(request, meeting_id):
        form = NoteForm(
            request.POST or None,
            initial={
                'meeting': meeting_id,
                'creator': request.user,
            })

        agenda_items = AgendaItem.objects.filter(meeting=Meeting.objects.get(pk=meeting_id)).order_by('-important_score')
        notes = []
        for agenda_item in agenda_items:
            notes.append({agenda_item: Note.objects.filter(agenda_item=agenda_item.pk)})

        if form.is_valid():
            form.save()
            return redirect(reverse('notes.create', args=(meeting_id, )))
        return render(request, 'notes/edit.html', {
            'form': form,
            'section': 'note',
            'notes_list': notes,
            'meeting_id': meeting_id
        })

    @login_required
    def edit(request, id):
        instance = get_object_or_404(Note, id=id)
        data = request.POST if request.method == 'POST' else None

        form = NoteForm(data, instance=instance)

        if form.is_valid():
            note = form.save()
            return redirect(reverse('notes.create', args=(instance.meeting.pk, )))

        return render(request, 'notes/edit_form.html', {
            'form': form,
            'form_action': '/meetings/notes/edit/' + str(id) + "/"
        })

    @login_required
    @csrf_exempt
    def delete(request, id: int):
        if request.method == 'POST':
            Note.objects.get(pk=id).delete()
            return HttpResponse(200)
        return HttpResponse(500)

    @login_required
    @csrf_exempt
    def mark_done(request, id: int):
        if request.method == 'POST':
            note = get_object_or_404(Note, id=id)
            note.is_done = True
            note.timestamp = datetime.datetime.now()
            note.save()
            return HttpResponse(200)
        return HttpResponse(500)

    @login_required
    @csrf_exempt
    def estimate_time(request, note_id):
        note = Note.objects.get(pk=note_id)
        form = TimeEstimateForm(
            request.POST or None,
            initial={
                'note': note,
                'user': request.user,
            })

        if form.is_valid():
            form.save()
            return redirect(reverse('notes.create', args=(note.meeting.id, )))

        return render(request, 'time_estimate/create.html', {
            'form': form,
            'form_action': '/meetings/time_estimate/create/' + str(note_id) + "/"
        })


class CommentView():
    @login_required
    def create(request, note_id):
        note = Note.objects.get(pk=note_id)
        form = CommentForm(
            request.POST or None,
            initial={
                'note': note,
                'creator': request.user,
            })

        if form.is_valid():
            form.save()
            return redirect(reverse('notes.create', args=(note.meeting.id, )))

        return render(request, 'comment/create.html', {
            'form': form,
            'form_action': '/meetings/comment/create/' + str(note_id) + "/"
        })


class TodoistView():
    @login_required
    def index(request):
        todoist_info = TodoistConnector.objects.get(connected_user=request.user.id)

        context = {
            "todoist_info": todoist_info
        }

        return render(request, 'todoist/index.html', context)

    @csrf_exempt
    def sync_meeting(request, meeting_id: int):
        from .src.todoist.controller import TodoistController
        controller = TodoistController(request.user)
        meeting = Meeting.objects.get(pk=meeting_id)

        if request.user not in meeting.participants.all():
            return HttpResponse("Done")

        try:
            project_name = meeting.meeting_redmine_project.name
        except AttributeError:
            project_name = meeting.title

        for note in Note.objects.filter(meeting=meeting):
            task_info = {
                "section": note.agenda_item.title,
                "priority": note.agenda_item.important_score,
                "content": note.text,
                "complete": note.is_done,
                "uncomplete": not note.is_done,
            }

            controller.update_or_create_task(project_name, task_info)

        return HttpResponse("Done")

class Pdf():
    @login_required
    def create(request, meeting_id):
        import os
        from django.conf import settings
        from django.http import HttpResponse
        from django.template import Context
        from django.template.loader import get_template
        import datetime
        from xhtml2pdf import pisa

        meeting = Meeting.objects.get(pk=meeting_id)

        data = {
            "meeting": meeting,
            "participants": meeting.participants.all(),
            "agenda_items": AgendaItem.objects.filter(meeting=meeting),
            "notes": Note.objects.filter(meeting=meeting),
        }

        template = get_template('pdf/default_meeting_report.html')
        html  = template.render(data)

        filename = settings.MEDIA_ROOT + "/pdfs/" + str(meeting.id) + "_" + str(meeting.title) + '.pdf'
        file = open(filename, "w+b")
        pisaStatus = pisa.CreatePDF(html.encode('utf-8'), dest=file, encoding='utf-8')

        file.seek(0)
        pdf = file.read()
        file.close()
        return pdf

    @login_required
    def print_note_request(request, meeting_id):
        pdf = Pdf.create(request, meeting_id)
        response = HttpResponse(pdf, content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="meeting_' + Meeting.objects.get(pk=meeting_id).title + '.pdf"'
        return response
