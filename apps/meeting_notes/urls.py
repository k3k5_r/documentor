from django.urls import path
from django.conf.urls import include, url
from . import views
from graphene_django.views import GraphQLView
from django.views.decorators.csrf import csrf_exempt

urlpatterns = [
    url(r"graphql", csrf_exempt(GraphQLView.as_view(graphiql=True))),

    path('meeting/index/', views.MeetingView.index, name="meetings.index"),
    path('meeting/create/', views.MeetingView.create, name="meetings.create"),
    path('meeting/edit/', views.MeetingView.edit),
    path('meeting/edit/<int:id>/', views.MeetingView.edit, name="meetings.edit"),
    path('meeting/delete/<int:id>/', views.MeetingView.delete, name="meetings.delete"),
    path('meeting/close/<int:id>/', views.MeetingView.close, name="meetings.close"),
    path('meeting/finish/<int:meeting_id>/', views.MeetingView.show_finish_modal, name="meetings.finish_modal"),
    path('meeting/send_mail/<int:meeting_id>/', views.MeetingView.send_invitation_mail, name="meetings.send_mail"),

    path('agenda_items/index/', views.AgendaItemView.index, name="agenda_item.index"),
    path('agenda_items/create/<int:meeting_id>', views.AgendaItemView.create, name="agenda_item.create"),
    path('agenda_items/edit/<int:agenda_item_id>/', views.AgendaItemView.edit, name="agenda_item.edit"),
    path('agenda_items/delete/<int:id>/', views.AgendaItemView.delete, name="agenda_item.delete"),
    path('agenda_items/done/<int:id>/', views.AgendaItemView.mark_done, name="agenda_item.mark_as_done"),

    path('notes/index/<int:meeting_id>/', views.NotesView.index, name="notes.index"),
    path('notes/own/', views.NotesView.own, name="notes.own"),
    path('notes/create/<int:meeting_id>/', views.NotesView.create, name="notes.create"),
    path('notes/edit/', views.NotesView.edit),
    path('notes/edit/<int:id>/', views.NotesView.edit, name="notes.edit"),
    path('notes/delete/<int:id>/', views.NotesView.delete, name="notes.delete"),
    path('notes/done/<int:id>/', views.NotesView.mark_done, name="notes.mark_as_done"),

    path('time_estimate/create/<int:note_id>/', views.NotesView.estimate_time, name="time_estimate.create"),

    path('comment/create/<int:note_id>/', views.CommentView.create, name="comment.create"),

    path('todoist/index/', views.TodoistView.index, name="todoist.index"),
    path('todoist/sync/<int:meeting_id>/', views.TodoistView.sync_meeting, name="todoist.sync"),

    path('pdf/print/<int:meeting_id>/', views.Pdf.print_note_request, name="pdf.create"),
]
