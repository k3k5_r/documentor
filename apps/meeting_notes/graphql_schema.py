import graphene
import graphql_jwt

import meeting_notes.graphql.agenda_item.schema as agenda_item_schema
import meeting_notes.graphql.meeting.schema as meeting_schema
import meeting_notes.graphql.note.schema as note_schema
import meeting_notes.graphql.user.schema as user_schema
import meeting_notes.graphql.time_estimate.schema as time_estimate_schema
import meeting_notes.graphql.sprint.schema as sprint_schema
import meeting_notes.graphql.sprint_user_settings.schema as sprint_user_settings_schema

class Query(agenda_item_schema.Query, meeting_schema.Query, note_schema.Query, user_schema.Query, time_estimate_schema.Query, sprint_schema.Query, sprint_user_settings_schema.Query, graphene.ObjectType):
    pass

class Mutation(user_schema.Mutation, meeting_schema.Mutation, agenda_item_schema.Mutation, note_schema.Mutation, graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()

schema = graphene.Schema(query=Query, mutation=Mutation)
