from ...models import Meeting, AgendaItem, Note, TimeEstimate, SprintUserSettings, Sprint
import datetime
from django.utils.timezone import localtime, now
from django.conf import settings
from enum import Enum
import json

class Days(Enum):
    MONDAY = 0
    TUESDAY = 1
    WEDNESDAY = 2
    THURSDAY = 3
    FRIDAY = 4
    SATURDAY = 5
    SUNDAY = 6

class SprintController(object):

    MINIMAL_TIME_INTERVAL = 0.5 # hours
    WEEKS_PER_YEAR = 52
    WEEK_LENGTH_IN_DAYS = 7

    def __init__(self, user):
        self.user = user
        self.user_settings = SprintUserSettings.objects.get(connected_user=self.user)

    def get_num_of_weeks_per_sprint(self):
        return self.user_settings.sprint_length_in_days // self.WEEK_LENGTH_IN_DAYS

    def get_num_of_sprints_per_year(self):
        return self.WEEKS_PER_YEAR // self.get_num_of_weeks_per_sprint()

    def get_weekly_work_time(self):
        return self.user_settings.work_time_mondays + self.user_settings.work_time_tuesdays + self.user_settings.work_time_wednesdays + \
            self.user_settings.work_time_thursdays + self.user_settings.work_time_fridays + self.user_settings.work_time_saturdays + \
            self.user_settings.work_time_sundays

    def get_total_slots_by_day(self, day):
        working_hours = 0

        if day == Days.MONDAY.value:
            working_hours = self.user_settings.work_time_mondays
        elif day == Days.TUESDAY.value:
            working_hours = self.user_settings.work_time_tuesdays
        elif day == Days.WEDNESDAY.value:
            working_hours = self.user_settings.work_time_wednesdays
        elif day == Days.THURSDAY.value:
            working_hours = self.user_settings.work_time_thursdays
        elif day == Days.FRIDAY.value:
            working_hours = self.user_settings.work_time_fridays
        elif day == Days.SATURDAY.value:
            working_hours = self.user_settings.work_time_saturdays
        elif day == Days.SUNDAY.value:
            working_hours = self.user_settings.work_time_sundays
        else:
            raise ValueError("Unknown day!")

        return (1 / self.MINIMAL_TIME_INTERVAL) * working_hours
    
    def get_available_time_slots_in_sprint(self):
        try:
            blocked = (int(self.user_settings.blocked_time_in_hours) * (1 / self.MINIMAL_TIME_INTERVAL))
        except TypeError:
            blocked = 0
        return (1 / self.MINIMAL_TIME_INTERVAL) * (self.get_weekly_work_time() * self.get_num_of_weeks_per_sprint()) - blocked

    def get_current_sprint_id(self):
        current_week = datetime.datetime.now().isocalendar()[1]
        current_sprint = current_week // self.get_num_of_weeks_per_sprint()
        return "sprint_" + str(current_sprint) + "/" + str(self.get_num_of_sprints_per_year()) + "/" + str(datetime.datetime.now().year)

    def get_sprint_start_and_end_date(self, start_date):
        start = start_date - datetime.timedelta(days=start_date.isoweekday() % 7)
        end = start - datetime.timedelta(days=self.user_settings.sprint_length_in_days)
        return {"start_date": start, "end_date": end}

    def get_start_and_end_for_current_sprint(self):
        return self.get_sprint_start_and_end_date(localtime(now()).date())

    @staticmethod
    def get_start_key(lst):
        return lst[3]

    def update_time_estimates_for_slots(self):
        estimates_to_update = TimeEstimate.objects.filter(user=self.user)
        for estimate in estimates_to_update:
            if estimate.time_estimate > 1:
                estimate.time_estimate_in_slots = (1 / self.MINIMAL_TIME_INTERVAL) * estimate.time_estimate
            else:
                estimate.time_estimate_in_slots = (1 / self.MINIMAL_TIME_INTERVAL) * (1 / estimate.time_estimate)
            estimate.save()
        return True

    def get_slot(self, slots, number_of_slots):
        for index, day in enumerate(slots):
            if sum(x is None for x in day) >= number_of_slots:
                return (index, day.index(None))
        return (0, 0)

    def set_items_to_time_slots(self):
        self.update_time_estimates_for_slots()

        working_hours = [self.get_total_slots_by_day(day) for day in range(Days.MONDAY.value, Days.SUNDAY.value + 1)] * int(self.get_num_of_weeks_per_sprint())

        slots = [[None] * int(hours) for hours in working_hours]
        last_blocked_slot_id = (0, 0) # format: day | slot

        current_timestamp_in_sprint = self.get_sprint_start_and_end_date(localtime(now()).date())
        
        estimates_to_plan = TimeEstimate.objects.filter(
            user=self.user, 
            timestamp__gte=current_timestamp_in_sprint.get('start_date')
        ).exclude(
            timestamp__lte=current_timestamp_in_sprint.get('end_date')
        ).order_by('-note__agenda_item__important_score')

        for estimate in estimates_to_plan:
            needed_slots = [estimate.id] * estimate.time_estimate_in_slots
            first_available_slot = self.get_slot(slots, len(needed_slots))
            while needed_slots:
                try:
                    if not slots[first_available_slot[0]][first_available_slot[1]]:
                        slots[first_available_slot[0]][first_available_slot[1]] = needed_slots.pop()
                    first_available_slot = (first_available_slot[0], first_available_slot[1] + 1)
                except IndexError:
                    first_available_slot = (first_available_slot[0] + 1, 0)

        sprint = Sprint.objects.get_or_create(
            connected_user=self.user, sprint_id=self.get_current_sprint_id()
        )

        sprint[0].tasks = None
        sprint[0].save()
        sprint[0].tasks = json.dumps(slots)
        sprint[0].save()

        return slots