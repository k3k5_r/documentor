from ...models import TodoistConnector
from todoist.api import TodoistAPI

class TodoistController(object):

    def __init__(self, user):
        self.connector = TodoistConnector.objects.get(connected_user=user.id)
        self.api = TodoistAPI(str(self.connector.api_token))
        self.api.sync()

    def parse_important_score_for_todoist(self, score: int):
        return int(score) + 1

    def find_project_id_by_name(self, project_name: str) -> int:
        all_projects = self.api.state['projects']
        for project in all_projects:
            if project_name == project['name']:
                return project['id']
        return None

    def find_section_id_by_project_and_name(self, project_id: int, section_name: str) -> int:
        all_sections = self.api.state['sections']
        for section in all_sections:
            if section['project_id'] == project_id and section['name'] == section_name:
                return section['id']
        return None

    def find_task_by_name(self, project_id: int, task_info: dict, section_id: int) -> int:
        all_items = self.api.state['items']
        for item in all_items:
            if item['project_id'] == int(project_id) and item['content'] == str(task_info.get('content')) and item['section_id'] == section_id:
                return item['id']
        return None

    def find_project_by_id(self, project_id: int) -> TodoistAPI:
        if type(project_id) != int:
            raise ValueError("Project ID must be an Integer!")
        return self.api.projects.get_by_id(int(project_id))

    def add_new_project(self, project_name: str) -> int:
        self.api.projects.add(str(project_name))
        self.api.commit()
        return self.find_project_id_by_name(str(project_name))

    def update_existing_project(self, project_name: str) -> bool:
        project_id = self.find_project_id_by_name(str(project_name))

        if not project_id:
            return False

        project = self.api.projects.get_by_id(int(project_id))
        project.update(name='foo')

        self.api.commit()

        return True

    def create_new_task(self, project_name: str, task_info: dict, section_id: int) -> bool:
        project_id = self.find_project_id_by_name(str(project_name))

        if not project_id:
            return False

        item = self.api.items.add(
            project_id=int(project_id),
            priority=self.parse_important_score_for_todoist(int(task_info.get('priority'))),
            content=task_info.get('content'),
            due=task_info.get('due'),
            section_id=section_id,
        )

        self.api.commit()
        self.api.sync()

        return True

    def create_new_section(self, project_id: int, section_name: str) -> bool:
        self.api.sections.add(str(section_name), project_id=project_id)
        self.api.commit()
        return self.find_section_id_by_project_and_name(project_id, section_name)

    def update_or_create_task(self, project_name: str, task_info: dict) -> bool:

        project_id = self.find_project_id_by_name(str(project_name))

        if not project_id:
            project_id = self.add_new_project(str(project_name))

        section_id = self.find_section_id_by_project_and_name(project_id, task_info.get('section'))

        if not section_id:
            section_id = self.create_new_section(int(project_id), task_info.get('section'))

        task_id = self.find_task_by_name(int(project_id), task_info, section_id)

        if not task_id:
            self.create_new_task(str(project_name), task_info, section_id)
            task_id = self.find_task_by_name(int(project_id), task_info, section_id)

        try:
            task = self.api.items.get_by_id(task_id)

            task.update(
                priority=self.parse_important_score_for_todoist(int(task_info.get('priority'))),
                content=task_info.get('content'),
                due=task_info.get('due'),
                assigned_by_uid=task_info.get('user_uid'),
                responsible_uid=task_info.get('responsible_uid'),
                section_id=section_id
            )

            if task_info.get('complete', False):
                task.complete()
            elif task_info.get('uncomplete', False):
                task.uncomplete()
            elif task_info.get('archive', False):
                task.archive()
            elif task_info.get('unarchive', False):
                task.unarchive()
        except IndexError:
            return True

        self.api.commit()

        return True
