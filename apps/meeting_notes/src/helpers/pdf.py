from ...models import Meeting, AgendaItem, Note, Comment

class PdfReporter(object):

    def __init__(self, user, meeting_id):
        self.user = user
        self.meeting = Meeting.objects.get(pk=meeting_id)
    
    def generatePdf(self):
        print(self.meeting)

    def generateHeader(self):
        pass

    def generateContent(self):
        pass
