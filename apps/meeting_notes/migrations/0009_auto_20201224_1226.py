# Generated by Django 2.2.17 on 2020-12-24 12:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('meeting_notes', '0008_auto_20201224_1210'),
    ]

    operations = [
        migrations.CreateModel(
            name='AgendaItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('important_score', models.CharField(choices=[('X', 'Not important'), ('.', 'Normal'), ('O', 'Important'), ('!', 'Very important')], default='.', max_length=1)),
                ('text', models.TextField(max_length=255)),
                ('meeting', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='meeting_notes.Meeting')),
            ],
        ),
        migrations.AddField(
            model_name='note',
            name='agenda_item',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='meeting_notes.AgendaItem'),
        ),
    ]
