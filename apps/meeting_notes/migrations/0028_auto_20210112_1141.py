# Generated by Django 2.2.17 on 2021-01-12 11:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('meeting_notes', '0027_sprint'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sprintusersettings',
            name='available_time_in_sprint',
        ),
        migrations.AddField(
            model_name='sprintusersettings',
            name='work_time_fridays',
            field=models.IntegerField(default=8),
        ),
        migrations.AddField(
            model_name='sprintusersettings',
            name='work_time_mondays',
            field=models.IntegerField(default=8),
        ),
        migrations.AddField(
            model_name='sprintusersettings',
            name='work_time_saturdays',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='sprintusersettings',
            name='work_time_sundays',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='sprintusersettings',
            name='work_time_thursdays',
            field=models.IntegerField(default=8),
        ),
        migrations.AddField(
            model_name='sprintusersettings',
            name='work_time_tuesdays',
            field=models.IntegerField(default=8),
        ),
        migrations.AddField(
            model_name='sprintusersettings',
            name='work_time_wednesdays',
            field=models.IntegerField(default=8),
        ),
    ]
