from django.apps import AppConfig


class MeetingNotesConfig(AppConfig):
    name = 'meeting_notes'
