from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from simple_history.models import HistoricalRecords

from redmine_connector.models import RedmineProject
from earned_value_analysis.models import ProjectSection

# Create your models here.
class Meeting(models.Model):
    MEETING_STATUS = [
        ('NEW', 'NEW'),
        ('PLANNED', 'PLANNED'),
        ('IN PROGRESS', 'IN PROGRESS'),
        ('DONE', 'DONE')
    ]
    status = models.CharField(
        max_length=20,
        choices=MEETING_STATUS,
        default='NEW'
    )
    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name="meeting_creator")
    participants = models.ManyToManyField(User, related_name="meeting_participant")
    invite_automatically_by_mail = models.BooleanField(default=True)
    title = models.CharField(max_length=255)
    description = models.TextField(max_length=5000, blank=True, null=True)
    start = models.DateTimeField()
    end = models.DateTimeField(default=None, null=True, blank=True)
    meeting_redmine_project = models.ForeignKey(RedmineProject, on_delete=models.CASCADE, default=None, blank=True, null=True)
    meeting_ticket_number = models.IntegerField(default=None, null=True, blank=True)
    auto_track_meeting_time = models.BooleanField(default=True)

    online_meeting_url = models.URLField(default=None, null=True, blank=True)
    offline_meeting_location = models.TextField(default=None, blank=True, null=True)

    is_closed = models.BooleanField(default=False)

    history = HistoricalRecords()

    def __str__(self):
        return self.title

class AgendaItem(models.Model):
    IMPORTANT_SCORE = [
        ('0', 'Not important'),
        ('1', 'Normal'),
        ('2', 'Important'),
        ('3', 'Very important')
    ]
    important_score = models.CharField(
        max_length=1,
        choices=IMPORTANT_SCORE,
        default='1'
    )
    meeting = models.ForeignKey(Meeting, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    text = models.TextField(max_length=5000, default=None, null=True, blank=True)
    project_section = models.ForeignKey(ProjectSection, on_delete=models.CASCADE, blank=True, null=True, default=None)

    is_done = models.BooleanField(default=False)
    done_timestamp = models.DateTimeField(blank=True, null=True, default=None)

    history = HistoricalRecords()

    def __str__(self):
        return str(self.id) + " | " + self.title

class Note(models.Model):
    TYPE_CHOICES = [
        ('NOTE', 'NOTE'),
        ('DECISION', 'DECISION'),
        ('OPTIONS', 'OPTIONS'),
        ('INFORMATION', 'INFORMATION'),
        ('PROGRESS', 'PROGRESS INFORMATION'),
        ('TBD', 'TO BE DECIDED')
    ]
    note_type = models.CharField(
        max_length=20,
        choices=TYPE_CHOICES,
        default='NOTE',
    )
    meeting = models.ForeignKey(Meeting, on_delete=models.CASCADE)
    agenda_item = models.ForeignKey(AgendaItem, on_delete=models.CASCADE, blank=True, null=True, default=None)
    project_section = models.ForeignKey(ProjectSection, on_delete=models.CASCADE, blank=True, null=True, default=None)
    text = models.TextField(max_length=5000)
    creator = models.ForeignKey(User, on_delete=models.CASCADE, related_name="note_creator")
    assigned_to = models.ForeignKey(User, on_delete=models.CASCADE, related_name="note_assignee", blank=True, null=True, default=None)
    connect_to_ticket = models.IntegerField(blank=True, null=True, default=None)
    is_private_note = models.BooleanField(default=False)
    timestamp = models.DateTimeField(auto_now=True)

    due_date = models.DateField(null=True, blank=True, default=None)

    is_done = models.BooleanField(default=False)
    done_timestamp = models.DateTimeField(blank=True, null=True, default=None)

    history = HistoricalRecords()

    is_dependent_on = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, default=None)

    def __str__(self):
        return self.creator.username + " / " + str(self.note_type) + " " + self.text[:10] + " (" + str(self.pk) + ")"


class Comment(models.Model):
    note = models.ForeignKey(Note, on_delete=models.CASCADE)
    text = models.TextField(max_length=5000)
    timestamp = models.DateTimeField(auto_now=True)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)


class TodoistConnector(models.Model):
    connected_user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    api_token = models.CharField(max_length=255)
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.connected_user.first_name + " " + self.connected_user.last_name


class TimeEstimate(models.Model):
    note = models.ForeignKey(Note, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    time_estimate = models.FloatField()
    time_estimate_in_slots = models.IntegerField(default=None, null=True, blank=True)


class SprintUserSettings(models.Model):
    connected_user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    sprint_length_in_days = models.IntegerField()
    blocked_time_in_hours = models.IntegerField(null=True, blank=True, default=None) # meant for weekends at 7 day sprints
    work_time_mondays = models.IntegerField(default=8)
    work_time_tuesdays = models.IntegerField(default=8)
    work_time_wednesdays = models.IntegerField(default=8)
    work_time_thursdays = models.IntegerField(default=8)
    work_time_fridays = models.IntegerField(default=8)
    work_time_saturdays = models.IntegerField(default=0)
    work_time_sundays = models.IntegerField(default=0)

    def __str__(self):
        return self.connected_user.username

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        SprintUserSettings.objects.create(
            connected_user=instance,
            sprint_length_in_days=7
        )

    
class Sprint(models.Model):
    connected_user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    sprint_id = models.CharField(max_length=100)
    tasks = models.CharField(max_length=5000, default=None, blank=True, null=True)

    def __str__(self):
        return self.connected_user.username + "_" + str(self.sprint_id)