import graphene
from graphene_django import DjangoObjectType

from meeting_notes.models import SprintUserSettings

from meeting_notes.graphql.meeting.schema import MeetingType
from meeting_notes.graphql.agenda_item.schema import AgendaItemType
from meeting_notes.graphql.note.schema import NoteType
from meeting_notes.graphql.user.schema import UserType

from meeting_notes.models import AgendaItem, Note, Meeting
from django.contrib.auth import get_user_model

class SprintUserSettingsType(DjangoObjectType):
    class Meta:
        model = SprintUserSettings
        fields = "__all__"

class Query(graphene.ObjectType):
    all_sprint_user_settings = graphene.List(SprintUserSettingsType)

    def resolve_sprint_user_settings_by_user(self, info, **kwargs):
        return SprintUserSettings.objects.get(user=info.user)
