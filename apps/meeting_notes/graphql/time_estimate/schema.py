import graphene
from graphene_django import DjangoObjectType

from meeting_notes.models import TimeEstimate

from meeting_notes.graphql.meeting.schema import MeetingType
from meeting_notes.graphql.agenda_item.schema import AgendaItemType
from meeting_notes.graphql.note.schema import NoteType
from meeting_notes.graphql.user.schema import UserType

from meeting_notes.models import AgendaItem, Note, Meeting
from django.contrib.auth import get_user_model

class TimeEstimateType(DjangoObjectType):
    class Meta:
        model = TimeEstimate
        fields = "__all__"

class Query(graphene.ObjectType):
    all_time_estimates = graphene.List(TimeEstimateType)

    def resolve_note_by_user(self, info, id, **kwargs):
        return TimeEstimate.objects.get(user=info.user)
