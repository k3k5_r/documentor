import graphene
import datetime
from graphene_django import DjangoObjectType

from meeting_notes.models import Meeting
from ..user.schema import UserType

class MeetingType(DjangoObjectType):
    class Meta:
        model = Meeting
        fields = "__all__"

class Query(graphene.ObjectType):
    all_meetings = graphene.List(MeetingType)
    meeting_by_id = graphene.Field(MeetingType, required=True, id=graphene.Int(required=True))
    meeting_by_title = graphene.List(MeetingType, required=True, title=graphene.String(required=True))

    def resolve_meeting_by_id(self, info, id, **kwargs):
        return Meeting.objects.get(id=id)

    def resolve_meeting_by_title(self, info, title, **kwargs):
        return Meeting.objects.filter(title=title)

    def resolve_all_meetings(self, info, **kwargs):
        return Meeting.objects.all()

class CreateMeeting(graphene.Mutation):
    status = graphene.String()
    title = graphene.String()
    description = graphene.String()
    start = graphene.DateTime()
    end = graphene.DateTime()
    meeting_redmine_project = graphene.Int()
    meeting_ticket_number = graphene.String()
    auto_track_meeting_time = graphene.Boolean()
    online_meeting_url = graphene.String()
    offline_meeting_location = graphene.String()

    class Arguments:
        status = graphene.String()
        title = graphene.String(required=True)
        description = graphene.String()
        start = graphene.DateTime(required=True)
        end = graphene.String()
        meeting_redmine_project = graphene.Int()
        meeting_ticket_number = graphene.String()
        auto_track_meeting_time = graphene.Boolean()
        online_meeting_url = graphene.String()
        offline_meeting_location = graphene.String()

    def mutate(self, info, title, **kwargs):
        meeting = Meeting(
            status=kwargs.get('status', "NEW"),
            title=title,
            description=kwargs.get('description', None),
            meeting_redmine_project=kwargs.get('meeting_redmine_project', None),
            meeting_ticket_number=kwargs.get('meeting_ticket_number', None),
            auto_track_meeting_time=kwargs.get('auto_track_meeting_time', False),
            online_meeting_url=kwargs.get('online_meeting_url', None),
            offline_meeting_location=kwargs.get('offline_meeting_location', None),
            creator=info.context.user,
            start=kwargs.get('start') if kwargs.get('start') else None,
            end=kwargs.get('end') if kwargs.get('end') else None,
        )
        meeting.save()

        return CreateMeeting(
            status=meeting.status,
            title=meeting.title,
            description=meeting.description,
            start=meeting.start,
            end=meeting.end,
            meeting_redmine_project=meeting.meeting_redmine_project,
            meeting_ticket_number=meeting.meeting_ticket_number,
            auto_track_meeting_time=meeting.auto_track_meeting_time,
            online_meeting_url=meeting.online_meeting_url,
            offline_meeting_location=meeting.offline_meeting_location,
        )


class Mutation(graphene.ObjectType):
    create_meeting = CreateMeeting.Field()
