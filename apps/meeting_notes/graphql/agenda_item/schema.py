import graphene
from graphene_django import DjangoObjectType

from meeting_notes.models import AgendaItem, Meeting

class AgendaItemType(DjangoObjectType):
    class Meta:
        model = AgendaItem
        fields = "__all__"

class Query(graphene.ObjectType):
    all_agenda_items = graphene.List(AgendaItemType)
    agenda_item_by_id = graphene.Field(AgendaItemType, required=True, id=graphene.Int(required=True))
    agenda_item_by_title = graphene.List(AgendaItemType, required=True, title=graphene.String(required=True))

    def resolve_agenda_item_by_id(self, info, id, **kwargs):
        return AgendaItem.objects.get(id=id)

    def resolve_agenda_item_by_title(self, info, title, **kwargs):
        return AgendaItem.objects.filter(title=title)

    def resolve_all_agenda_items(self, info, **kwargs):
        return AgendaItem.objects.all()

class CreateAgendaItem(graphene.Mutation):
    important_score = graphene.String()
    meeting = graphene.ID()
    title = graphene.String()
    text = graphene.String()
    project_section = graphene.ID()
    is_done = graphene.Boolean()
    done_timestamp = graphene.String()

    class Arguments:
        meeting = graphene.Int(required=True)
        title = graphene.String(required=True)
        important_score = graphene.String()
        text = graphene.String()
        project_section = graphene.ID()
        is_done = graphene.Boolean()
        done_timestamp = graphene.String()

    def mutate(self, info, meeting, title, **kwargs):
        meeting_obj = Meeting.objects.get(id=int(meeting))
        agenda_item = AgendaItem(
            meeting=meeting_obj,
            title=title,
            important_score=kwargs.get('important_score', "."),
            text=kwargs.get('text', None),
            project_section=kwargs.get('project_section', None),
            is_done=kwargs.get('is_done', False),
            done_timestamp=kwargs.get('done_timestamp', None)
        )
        agenda_item.save()

        return CreateAgendaItem(
            important_score=agenda_item.important_score,
            meeting=agenda_item.meeting,
            title=agenda_item.title,
            text=agenda_item.text,
            project_section=agenda_item.project_section,
            is_done=agenda_item.is_done,
            done_timestamp=agenda_item.done_timestamp
        )


class Mutation(graphene.ObjectType):
    create_agenda_item = CreateAgendaItem.Field()
