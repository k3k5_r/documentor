import graphene
from graphene_django import DjangoObjectType

from meeting_notes.models import Sprint

from meeting_notes.graphql.meeting.schema import MeetingType
from meeting_notes.graphql.agenda_item.schema import AgendaItemType
from meeting_notes.graphql.note.schema import NoteType
from meeting_notes.graphql.user.schema import UserType

from meeting_notes.models import AgendaItem, Note, Meeting
from django.contrib.auth import get_user_model

class SprintType(DjangoObjectType):
    class Meta:
        model = Sprint
        fields = "__all__"

class Query(graphene.ObjectType):
    all_sprints = graphene.List(SprintType)

    def resolve_sprint_by_user(self, info, id, **kwargs):
        return Sprint.objects.filter(connected_user=info.user)

    def resolve_sprint_by_sprint_id(self, info, sprint_id, **kwargs):
        return Sprint.objects.filter(sprint_id=sprint_id)

    def resolve_sprint_by_sprint_id_and_user(self, info, sprint_id, **kwargs):
        return Sprint.objects.filter(connected_user=info.user, sprint_id=sprint_id)
