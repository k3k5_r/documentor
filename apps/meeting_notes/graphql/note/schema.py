import graphene
from graphene_django import DjangoObjectType

from meeting_notes.models import Note

from meeting_notes.graphql.meeting.schema import MeetingType
from meeting_notes.graphql.agenda_item.schema import AgendaItemType
from meeting_notes.graphql.user.schema import UserType

from meeting_notes.models import AgendaItem, Note, Meeting
from django.contrib.auth import get_user_model

class NoteType(DjangoObjectType):
    class Meta:
        model = Note
        fields = "__all__"

class Query(graphene.ObjectType):
    all_notes = graphene.List(NoteType)
    meetings = graphene.List(MeetingType)

    def resolve_meetings(self, info, **kwargs):
        return Meeting.objects.all()

    def resolve_note_by_id(self, info, id, **kwargs):
        return Note.objects.get(id=id)

    def resolve_note_by_title(self, info, title, **kwargs):
        return Note.objects.filter(title=title)

    def resolve_all_notes(self, info, **kwargs):
        return Note.objects.all()

class CreateNote(graphene.Mutation):
    meeting = graphene.Field(MeetingType)
    note_type = graphene.String()
    agenda_item = graphene.Field(AgendaItemType)
    project_section = graphene.ID()
    text = graphene.String()
    creator = graphene.Field(UserType)
    assigned_to = graphene.Field(UserType)
    connect_to_ticket = graphene.String()
    is_private_note = graphene.Boolean()
    timestamp = graphene.DateTime()
    is_done = graphene.Boolean()
    done_timestamp = graphene.DateTime()

    class Arguments:
        note_type = graphene.String()
        project_section = graphene.ID()
        text = graphene.String()
        connect_to_ticket = graphene.String()
        is_private_note = graphene.Boolean()
        timestamp = graphene.DateTime()
        is_done = graphene.Boolean()
        done_timestamp = graphene.DateTime()
        meeting_id = graphene.Int()
        creator_id = graphene.Int()
        assigned_to_id = graphene.Int()
        agenda_item_id = graphene.Int()

    def mutate(self, info, meeting_id, assigned_to_id=None, creator_id=None, agenda_item_id=None, **kwargs):
        note = Note(
            note_type = kwargs.get('note_type', 'NOTE'),
            project_section = kwargs.get('project_section', None),
            text = kwargs.get('text', None),
            creator = info.context.user,
            assigned_to = kwargs.get('assigned_to', None),
            connect_to_ticket = kwargs.get('connect_to_ticket', None),
            is_private_note = kwargs.get('is_private_note', False),
            timestamp = kwargs.get('timestamp', None),
            is_done = kwargs.get('is_done', False),
            done_timestamp = kwargs.get('done_timestamp', None),
            meeting=Meeting.objects.filter(id=meeting_id).first(),
        )

        if creator_id:
            note.creator=User.objects.filter(id=creator_id).first()

        if assigned_to_id:
            note.assigned_to=User.objects.filter(id=assigned_to_id).first()

        if agenda_item_id:
            note.agenda_item=AgendaItem.objects.filter(id=agenda_item_id).first()

        note.save()

        return CreateNote(
            meeting = note.meeting,
            note_type = note.note_type,
            agenda_item = note.agenda_item,
            project_section = note.project_section,
            text = note.text,
            creator = note.creator,
            assigned_to = note.assigned_to,
            connect_to_ticket = note.connect_to_ticket,
            is_private_note = note.is_private_note,
            timestamp = note.timestamp,
            is_done = note.is_done,
            done_timestamp = note.done_timestamp,
        )


class Mutation(graphene.ObjectType):
    create_note = CreateNote.Field()
