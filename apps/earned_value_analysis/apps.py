from django.apps import AppConfig


class EarnedValueAnalysisConfig(AppConfig):
    name = 'earned_value_analysis'
