from django.shortcuts import render
from django.http import HttpResponse
from django.core.paginator import Paginator
from django.contrib.auth.decorators import login_required

from .src.analysis import EVAController
from .models import EarnedValueAnalysis
import redmine_connector

# Create your views here.
@login_required
def index(request, slug="swa"):
    eva = EarnedValueAnalysis.objects.filter(project__exact=slug)
    paginator = Paginator(eva, 1)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'analysis/index.html', {'page_obj': page_obj})

@login_required
def calculate(request):
    instance = EVAController()
    display = dict(instance.redmine_instance.project)
    return HttpResponse("You're looking at question %s." % str(display))

@login_required
def calculate_all(request):
    instances = redmine_connector.models.RedmineProject.objects.all()
    for proj in instances:
        instance = EVAController(proj.project_slug)

    return HttpResponse("You're looking at question %s." % str("test"))
