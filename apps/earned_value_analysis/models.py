from django.db import models
from datetime import datetime

import redmine_connector.models as redmine_connector

# Create your models here.
class ProjectMetaInformation(models.Model):
    project = models.ForeignKey(redmine_connector.RedmineProject, on_delete=models.CASCADE)
    start = models.DateTimeField()
    end = models.DateTimeField(default=None, null=True, blank=True)
    weeks = models.IntegerField(default=None, null=True, blank=True)
    time_budget = models.IntegerField(default=None, null=True, blank=True)
    money_budget = models.IntegerField(default=None, null=True, blank=True)
    hourly_rate = models.IntegerField(default=None, null=True, blank=True)
    weekly_budget = models.IntegerField(default=None, null=True, blank=True)

class EarnedValueAnalysis(models.Model):
    project = models.ForeignKey(redmine_connector.RedmineProject, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(default=datetime.now)
    planned_completion = models.FloatField(default=None, null=True, blank=True)
    actual_completion = models.FloatField(default=None, null=True, blank=True)
    planned_costs = models.FloatField(default=None, null=True, blank=True)
    actual_costs = models.FloatField(default=None, null=True, blank=True)
    planned_value = models.FloatField(default=None, null=True, blank=True)
    earned_value = models.FloatField(default=None, null=True, blank=True)
    scheduled_variance = models.FloatField(default=None, null=True, blank=True)
    cost_variance = models.FloatField(default=None, null=True, blank=True)
    cost_performance_index = models.FloatField(default=None, null=True, blank=True)
    scheduled_performance_index = models.FloatField(default=None, null=True, blank=True)

    class Meta:
        get_latest_by = "timestamp"

class ProjectSection(models.Model):
    project = models.ForeignKey(redmine_connector.RedmineProject, on_delete=models.CASCADE, blank=True, null=True, default=None)
    title = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True, default=None, max_length=5000)
    catalog_id = models.IntegerField(blank=True, null=True, default=None)
    catalog_url = models.URLField(blank=True, null=True, default=None)

    def __str__(self):
        return self.title
