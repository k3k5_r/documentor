from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(ProjectMetaInformation)
admin.site.register(EarnedValueAnalysis)
admin.site.register(ProjectSection)
