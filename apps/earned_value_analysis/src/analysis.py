from django import db
import pandas as pd
from datetime import datetime

from redminelib import Redmine as RedmineAPI

from ..models import *
from redmine_connector.src.connector import RedmineConnector

class EVAController(object):

    def __init__(self, slug):
        self.redmine_instance = RedmineConnector(slug)
        self.clean_eva_and_meta()
        try:
            self.parse_time_entries()
            self.get_progress()
            self.set_attributes()
        except:
            pass

    def get_progress(self):
        ticket_status = {"open": 0, "progress": 0, "done": 0, "sum_of_tickets": 0}

        issues = self.redmine_instance.connection.issue.filter(project_id=self.redmine_instance.local_project.project_slug, status_id='*')

        for issue in issues:
            if issue.status.id in [1, 7]:
                ticket_status["open"] += 1
                ticket_status["sum_of_tickets"] += 1
            elif issue.status.id in [2, 4, 10, 9, 12]:
                ticket_status["progress"] += 1
                ticket_status["sum_of_tickets"] += 1
            elif issue.status.id in [3, 5]:
                ticket_status['done'] += 1
                ticket_status["sum_of_tickets"] += 1
            else:
                ticket_status["sum_of_tickets"] += 1

        self.current_progress = (ticket_status.get('open') * 1 + ticket_status.get('progress') * 40 + ticket_status.get('done') * 100) / ticket_status.get('sum_of_tickets', 1)

    def parse_time_entries(self):
        all_times = list()
        for entry in self.redmine_instance.project.time_entries:
            current = dict()
            current["issue"] = entry.issue.id
            current["activity"] = entry.activity.name
            current["hours"] = entry.hours
            current["spent_on"] = entry.spent_on
            current["spent_on_week"] = entry.spent_on.isocalendar()[1]
            all_times.append(current)

        self.project_time_entries = pd.DataFrame(all_times).set_index('spent_on')

    def clean_eva_and_meta(self):
        ProjectMetaInformation.objects.filter(project=self.redmine_instance.local_project).delete()
        EarnedValueAnalysis.objects.filter(project=self.redmine_instance.local_project).delete()

    def set_attributes(self):
        meta = ProjectMetaInformation(
            project=self.redmine_instance.local_project,
            start=datetime.strptime(self.redmine_instance.custom_fields[5].get('value'), '%Y-%m-%d'),
            end=datetime.strptime(self.redmine_instance.custom_fields[6].get('value'), '%Y-%m-%d'),
            time_budget=int(self.redmine_instance.custom_fields[7].get('value')),
            money_budget=int(self.redmine_instance.custom_fields[2].get('value')),
        )

        meta.weeks = (meta.end - meta.start).days // 7
        meta.hourly_rate = meta.money_budget / meta.time_budget
        meta.weekly_budget = meta.time_budget / meta.weeks
        meta.save()

        index = 1
        for weekno in range(meta.start.isocalendar()[1], meta.end.isocalendar()[1], 1):
            current = EarnedValueAnalysis(
                project = self.redmine_instance.local_project,
                timestamp = datetime.strptime(str(meta.start.year) + "-W" + str(weekno) + '-1', "%Y-W%W-%w"),
                planned_completion = (index / meta.weeks),
                actual_completion = self.current_progress / 100,
                actual_costs = sum([value for key, value in self.get_spent_budget_by_week(weekno).items()]),
                planned_costs = meta.time_budget / (index / meta.weeks),
            )

            current.planned_value = current.planned_completion * meta.time_budget
            current.earned_value = current.actual_completion * meta.time_budget
            current.scheduled_variance = current.earned_value - current.planned_value
            current.cost_variance = current.earned_value - current.actual_costs
            try:
                current.cost_performance_index = current.earned_value / current.actual_costs
                current.scheduled_performance_index = current.earned_value / current.planned_value
            except ZeroDivisionError:
                current.cost_performance_index = 0.0
                current.scheduled_performance_index = 0.0
            current.save()
            index += 1

    def get_spent_budget_by_week(self, weekno: int):
        return dict(self.project_time_entries.loc[self.project_time_entries['spent_on_week'] == weekno].groupby('activity').sum()['hours'])
