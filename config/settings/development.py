# Python imports
import os

# fetch the common settings
from .common import *

# ##### APPLICATION CONFIGURATION #########################

# allow all hosts during development
ALLOWED_HOSTS = ['*']

INSTALLED_APPS = DEFAULT_APPS


# ##### DATABASE CONFIGURATION ############################
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_ROOT, 'run', 'dev.sqlite3'),
    }
}

EMAIL_HOST = 'smtp.mailtrap.io'
EMAIL_HOST_USER = os.environ.get('MAILTRAP_USERNAME', '10af006b657285')
EMAIL_HOST_PASSWORD = os.environ.get('MAILTRAP_PASSWORD', '8ee9d8cb6ab8bb')
EMAIL_PORT = '2525'


# ##### DEBUG CONFIGURATION ###############################
DEBUG = True
