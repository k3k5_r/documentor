"""project_management URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
"""
# Django imports
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

import redmine_connector.views as rc_views
import earned_value_analysis.views as eva_views

urlpatterns = [
    # Examples:
    # url(r'^blog/', include('blog.urls', namespace='blog')),

    path('', rc_views.DashboardView.index, name="dashboard.index"),
    path('gantt/', rc_views.DashboardView.gantt_data, name="dashboard.gantt_data"),

    # provide the most basic login/logout functionality
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),

    # enable the admin interface
    url(r'^admin/', admin.site.urls, name="admin_view"),

    url(r'^connector/index', rc_views.index),
    url(r'^connector/all_projects', rc_views.get_all_redmine_projects),
    path('analysis/calculate', eva_views.calculate),
    path('analysis/calculate/all', eva_views.calculate_all),
    path('analysis/index/<slug:slug>', eva_views.index),
    path('meetings/', include('meeting_notes.urls'))
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
